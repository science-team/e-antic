e-antic (2.0.2+ds-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Doug Torrance <dtorrance@debian.org>  Tue, 02 Apr 2024 21:45:02 -0400

e-antic (2.0.2+ds-2~exp2) experimental; urgency=medium

  * Debianization:
    - d/control:
      - Build-Depends list, force libflint-dev version greater than 3.1.2.

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 29 Mar 2024 13:35:18 +0000

e-antic (2.0.2+ds-2~exp1) experimental; urgency=medium

  * FTBFS fix release due to #1067226 which was fixed
    in experimental(Closes: #1067262).

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 29 Mar 2024 08:57:48 +0000

e-antic (2.0.2+ds-1) unstable; urgency=medium

  * New upstream patch version.
  * Debianization:
    - d/watch, correct;
    - d/patches/*:
      - d/p/gcc_13.patch, obsoleted;
    - d/control:
      - SONAME, bump to 3;
    - debian/copyright:
      - copyright year tuples, update.

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 23 Feb 2024 18:55:04 +0000

e-antic (2.0.0+ds1-2) unstable; urgency=medium

  * Drop dependency on flint-arb and antic.

 -- Julien Puydt <jpuydt@debian.org>  Tue, 19 Dec 2023 14:53:40 +0100

e-antic (2.0.0+ds1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release (Closes: #1056094).

 -- Julien Puydt <jpuydt@debian.org>  Mon, 18 Dec 2023 11:16:00 +0100

e-antic (1.3.0+ds1-1) unstable; urgency=medium

  * FTBFS fix release, migration to catch v3 has not yet been performed.
  * Debianization:
    - d/{copyright,control}, now use catch2 material as provided by
        the e-antic upstream team (Closes: #1054706);
    - d/watch, allow Debian Source suffix with a numerical index;
    - d/p/debianization-no_external.patch, obsoleted;
    - d/control:
      - Standards-Version, bump to version 4.6.2 (no change).

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 04 Nov 2023 18:57:20 +0000

e-antic (1.3.0+ds-1) unstable; urgency=medium

  * New upstream minor version.
  * Debianization:
    - d/control:
      - Standards-Version, bump to version 4.6.1 (no change);
    - d/libeantic1.symbols, update;
    - d/copyright:
      - URI url, correct;
    - d/s/lintian-overrides, update.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 11 Dec 2022 16:45:43 +0000

e-antic (1.2.1+ds-2) unstable; urgency=medium

  * Debianization:
    - d/watch, now use GitHub API to avoid breaking change from GitHub web UI
      (see https://lists.debian.org/debian-devel/2022/09/msg00229.html);

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 03 Dec 2022 15:22:40 +0000

e-antic (1.2.1+ds-1) unstable; urgency=medium

  * New upstream nano version.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 22 May 2022 17:02:22 +0000

e-antic (1.2.0+ds-1) unstable; urgency=medium

  * New upstream minor version.
  * Debianization:
    - d/patches/*:
      - d/p/adhoc-upstream-missing-script_version_maps.patch, obsoleted.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 24 Apr 2022 14:19:08 +0000

e-antic (1.1.0+ds-3) unstable; urgency=medium

  * Debianization:
    - d/patches/*:
      - d/p/upstream-harden-local_header.patch, introduce.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 27 Feb 2022 22:28:30 +0000

e-antic (1.1.0+ds-2) unstable; urgency=medium

  * Debianization:
    - d/control:
      - Build-Depends field, add libbenchmark-dev;
      - libeantic-dev Depends field, add libantic-dev and libboost-dev.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 27 Feb 2022 17:13:52 +0000

e-antic (1.1.0+ds-1) unstable; urgency=medium

  * New major upstream version (Closes: #990640).
  * Debianization:
    - d/copyright:
      - Files-Excluded list, refresh;
      - wise update;
    - d/watch, correct;
    - d/control:
      - SONAME, increment;
    - d/rules:
      - override_dh_auto_configure target, update;
      - DEB_HOST_MULTIARCH env variable, export;
    - d/patches/*:
      - d/p/upstream-libtool-version_script.patch, integrated;
      - d/p/upstream-libtool-versioning.patch, obsoleted;
      - d/p/adhoc-upstream-missing-script_version_maps.patch, introduce;
      - d/p/debianization*.patch, refresh;
    - d/not-installed, introduce;
    - d/upstream/metadata, introduce;
    - d/libeantic1.symbols, update.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 20 Feb 2022 18:50:14 +0000

e-antic (0.1.8+ds-2) unstable; urgency=medium

  [ Peter Michael Green ]
  * Apply patches from https://github.com/flatsurf/e-antic/commits/master0
    for flint 2.7 and above (Closes: #993269).

  [ Doug Torrance ]
  * debian/control
    - Bump Standards-Version to 4.6.0.
  * debian/salsa-ci.yml
    - Add Salsa pipeline config file.

  [ Jerome Benoit ]
  * debian/watch, refresh.
  * debian/copyright:
    - Source field, update.
  * debian/control:
    - Homepage field, update.

 -- Jerome Benoit <calculus@rezozer.net>  Wed, 22 Sep 2021 13:53:41 +0000

e-antic (0.1.8+ds-1) unstable; urgency=medium

  * Debianization:
    - debian/watch, refresh;
    - debian/libeantic0.symbols, discard debian revison suffix;
    - debian/control:
      - Standards Version, bump to 4.5.1 (no change);
    - debian/copyright:
      - copyright year tuples, update.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 21 Feb 2021 14:17:56 +0000

e-antic (0.1.8+ds-0.1) unstable; urgency=medium

  * Non-maintainer upload.
  * New upstream release 0.1.8.
  * Made b-dep on flint and flint-arb unconditional (Closes: #963258).
  * Fix team mailing list address (Closes: #970754).
  * Refresh patches (and drop some obsolete ones).
  * Update d/copyright.
  * Bump dh compat to 13.

 -- Julien Puydt <jpuydt@debian.org>  Mon, 12 Oct 2020 09:28:30 +0200

e-antic (0.1.5+ds-2.1) unstable; urgency=medium

  [ Gianfranco Costamagna ]
  * Non-maintianer upload

  [ Peter Michael Green ]
  * Fix build with flint 2.6.3 (Closes: 963290 )
    + Apply upstream commit 10ed02f429f75a418ee41814af2dffc8cd41101f
      as debian/patches/flint-2.6.0.patch to support flint 2.6.0
    + Apply upstream commit cebabe52632013a70be321d590301e06c306a766
      as debian/patches/remove-flint-upperlimit.patch to allow builds
      with newer flint versions.
    + Disable debian/patches/upstream-fix-sprintf_buffer_overflow.patch
      it conflicts with the flint 2.6.0 patch and according to
      https://github.com/videlec/e-antic/pull/92 the issue it addresses
      was fixed as part of that patch.
    + Add EANTIC_FIXED_fmpq_poly_add_fmpq to libeantic.map in
      upstream-libtool-version_script.patch
    + Update symbols file, removed symbols do not appear to be
      used by any other packages in Debian.

 -- Peter Michael Green <plugwash@raspbian.org>  Thu, 27 Aug 2020 15:59:03 +0000

e-antic (0.1.5+ds-2) unstable; urgency=medium

  * Serious fix release, revert symbols (Closes: #960614, #960875).

 -- Jerome Benoit <calculus@rezozer.net>  Tue, 19 May 2020 12:45:32 +0000

e-antic (0.1.5+ds-1) unstable; urgency=medium

  * New upstream version.
  * Debianization:
    - debian/copyright:
      - copyright year tuples, update;
    - debian/control:
      - Maintainer address, update;
      - Rules-Requires-Root, introduce and set to no;
      - Standards Version, bump to 4.5.0 (no change);
    - debian/patches/*:
      - d/p/upstream-fix-sprintf_buffer_overflow.patch , introduce
        (Closes: #943824).

 -- Jerome Benoit <calculus@rezozer.net>  Tue, 05 May 2020 05:13:25 +0000

e-antic (0.1.3+ds-3) unstable; urgency=medium

  * FTBFS fix release (Closes: #941259), see below.
  * Debianization:
    - debian/{control,rules}, permit alpha port for flint.

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 27 Sep 2019 16:18:49 +0000

e-antic (0.1.3+ds-2) unstable; urgency=medium

  * Upload to unstable.
  * Debianization:
    - debian/control:
      - Standards Version, bump to 4.4.0 (no change).

 -- Jerome Benoit <calculus@rezozer.net>  Thu, 11 Jul 2019 15:35:47 +0000

e-antic (0.1.3+ds-1) experimental; urgency=medium

  * New upstream micro version.
  * Debianization:
    - debian/patches/*:
      - d/p/upstream-fix-test-poly_extra-32bitarch.patch, obsolete;
      - d/p/upstream-headers-*.patch, integrated;
      - d/p/adhoc-fix-test-randtest.patch, obsolete.

 -- Jerome Benoit <calculus@rezozer.net>  Thu, 20 Jun 2019 18:49:21 +0000

e-antic (0.1.2+ds-4) experimental; urgency=medium

  * Debianization:
    - debian/control:
      - Homepage field, refresh;
      - libeantic-dev, Depends field, complete.

 -- Jerome Benoit <calculus@rezozer.net>  Thu, 06 Jun 2019 19:06:05 +0000

e-antic (0.1.2+ds-3) experimental; urgency=medium

  * FTBFS fix release (Closes: #929788), see below.
  * Debianization:
    - debian/patches/*:
      - d/p/upstream-fix-test-poly_extra-32bitarch.patch , import from
        upstream repository;
      - d/p/adhoc-fix-test-randtest.patch , introduce.

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 31 May 2019 08:00:01 +0000

e-antic (0.1.2+ds-2) experimental; urgency=medium

  * Debianization:
    - debian/control:
      - Build-Depends field, discard valgrind;
    - debian/rules:
      - OpenMP support, manage.

 -- Jerome Benoit <calculus@rezozer.net>  Thu, 23 May 2019 13:35:39 +0000

e-antic (0.1.2+ds-1) experimental; urgency=medium

  * Initial release. (Closes: #928236)

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 18 May 2019 07:09:23 +0000
